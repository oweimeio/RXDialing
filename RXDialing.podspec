#
#  Be sure to run `pod spec lint RXDialing.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see https://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |spec|

  # ―――  Spec Metadata  ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  These will help people to find your library, and whilst it
  #  can feel like a chore to fill in it's definitely to your advantage. The
  #  summary should be tweet-length, and the description more in depth.
  #

  spec.name         = "RXDialing"
  spec.version      = "2.0.0"
  spec.summary      = "容信VoIP插件"

  spec.description  = <<-DESC
                  容联云通讯容信插件VoIP模块，依赖RXAppModel,RXCommon
                   DESC

  spec.homepage     = "https://www.yuntongxun.com/"
  spec.license      = "MIT"
  spec.author       = { "gaoyuan" => "2502905737@qq.com" }
  spec.ios.deployment_target = "9.0"
  spec.source          = { :git => "https://gitlab.com/RXPlugins/RXDialing.git", :tag => "#{spec.version}" }
  spec.resources       = ["source/VoipResource/*.{bundle,caf,db}","source/VoipResource/xib/*.xib","source/VoipResource/xib/**/*.xib","source/**/*.png"]
  spec.framework       = "PushKit"
  spec.source_files    = "header/*.h"
  spec.vendored_library = 'lib/*.a'
  spec.requires_arc = true
  spec.dependency "RXCommon"
  spec.dependency "RXAppModel"

end
