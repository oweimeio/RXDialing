//
//  Dialing.hDelegate
//  ccp_ios_kit
//
//  Created by wangming on 15/7/10.
//  Copyright (c) 2015年 ronglian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "ECDeviceKitDelegateBase.h"
/**
 * 该代理用于处理界面的回调消息
 */
@protocol ECKitDeviceDelegate <ECDeviceKitDelegateBase>

@end