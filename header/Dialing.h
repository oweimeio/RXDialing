//
//  Dialing.h
//  Dialing
//
//  Created by wangming on 16/7/26.
//  Copyright © 2016年 ronglian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "BaseComponent.h"
#import "ECKitDeviceDelegate.h"

#define Table_User_mobile            @"mobile"            //'手机号码',

typedef void (^didFinish)(NSString *tel);

typedef void (^didFail)(NSError *error);

@interface Dialing : BaseComponent <UIApplicationDelegate>

//SYNTHESIZE_SINGLETON_FOR_CLASS_HEADER(Dialing);
+ (Dialing *)sharedInstance;

@property(nonatomic, copy) NSString *userId;
@property(nonatomic, copy) NSString *userPhone;
@property(nonatomic, copy) NSString *userName;

@property(nonatomic, strong) id callViewController;
@property(nonatomic, strong) id answerTelephoneViewController;
@property(nonatomic, strong) id videoViewController;

@property(nonatomic, strong) UIViewController * rxVoIPCallController;

/**
 @brief device代理
 @discussion 用于监听通知事件
 */
@property(nonatomic, assign) id <ECKitDeviceDelegate> delegate;

/**
 @brief 获取通话记录界面
 @discussion 调用后启动通话界面
 @return 无返回值
 */

- (UIViewController *)getDialingViewWithViewController;

/**
 @brief 获取通话界面
 @discussion 根据传入的参数获取通话界面
 @param caller 被叫voip号码或者用户Id
 @param callerNickname 被叫昵称
 @param callType 呼叫类型
 @param  callDirect;呼叫方向
 @return
 */
- (id)startCallViewWithDict:(NSDictionary *)dict;

/**
 @brief 显示选择菜单
 @param dict类型 phone电话 ，nickname昵称,memberId account账号
 @param owner 容器，从哪个容器中弹出通话界面
 */
- (id)showViewSelectMenu:(NSDictionary *)dict andOwner:(UIViewController *)owner;

/**
 * 异常情况退出通话 此处估计没有效果 先不用
 */
- (id)terminateHanUpCall;

@end
